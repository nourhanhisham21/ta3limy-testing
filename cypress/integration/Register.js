describe('Register', function() {

  beforeEach(function(){
    cy.fixture('example').then(function(data){
      this.data=data
      debugger

    })

  })
    
    it('Visits ta3limy.com', function() 
    {
      cy.visit_url()
      cy.title().should('be.equal','منصة تعليمي - منصات تعليمية - ڤودافون مصر لتنمية المجتمع')
    })
    
    it('register as a parent', function() 
    {
      
        cy.register()

    })

    it('insert info', function() 
    {
      cy.get("#firstName").type(this.data.Firstname)
      cy.get("#lastName").type(this.data.Secondname)
      cy.get("#mobileNumber").type(this.data.Mobile,{force:true})
      

    })

    it('select gender', function() 
    { 
      
      cy.wait(2000)
        cy.get("#female").click({ force: true })
      

    })

    it('select age', function() 
    {
      
      cy.wait(2000)
        cy.get("#ageRange").select('أقل من 25 سنة',{force:true})
     

    })
    
    it('insert password', function() 
    {
      
      cy.get("#password").type(this.data.Password,{force:true})
      cy.get("#passwordConfirmation").type(this.data.Password,{force:true})
      
    })

    it('check terms and conditions box', function() 
    {
      cy.get("#termsAndConditionsCheck").click({force:true})


    })

    it('check reCaptcha', function() 
    {
      
      cy.get('iframe').then(($iframe) => {
        const doc = $iframe.contents()
        doc.find('#recaptcha-anchor').click(()=>{
          console.log('click')  
        })
        
        cy.wrap(doc.find('#recaptcha-anchor')).click({force:true})
        
})


 
    })

    it('submit', function() 
    {
      
       cy.get('form[class="css-1xdhyk6 e11pp5v82"]').submit()


    })

    })

    